using System;

namespace ExifRestore
{
    public class PhotoInfo
    {
        public string title { get; set; }
        public GeoData geoData { get; set; }
        public GeoData geoDataExif { get; set; }
        public Time creationTime { get; set; }
        public Time photoTakenTime { get; set; }
        public Time photoLastModifiedTime { get; set; }

        public class GeoData
        {
            public double latitude { get; set; }
            public double longitude { get; set; }
            public double altitude { get; set; }
            public double latitudeSpan { get; set; }
            public double longitudeSpan { get; set; }
        }
        public class Time
        {
            public Int64 timestamp { get; set; }
            public DateTime formatted { get; set; }
        }
    }
}