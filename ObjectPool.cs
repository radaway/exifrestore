using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace ExifRestore
{
    public abstract class PoolableObject
    {
        public int InstanceId { get; set; }
        public bool Initialized { get; set; }
        public bool IsReady { get; set; }
    }

    public class ObjectPool<T> where T : PoolableObject
    {
        readonly ConcurrentBag<T> _objects;
        readonly Func<Task<T>> _objectGenerator;
        readonly SemaphoreSlim _semaphore;
        readonly CancellationToken _ct;
        int _instanceCount = 0;
        int _maxPoolSize = 0;
        public ObjectPool(Func<Task<T>> objectGenerator, int poolSize, CancellationToken ct)
        {
            _objectGenerator = objectGenerator ?? throw new ArgumentNullException(nameof(objectGenerator));
            _objects = new ConcurrentBag<T>();
            _semaphore = new SemaphoreSlim(poolSize);
            _ct = ct;
            _maxPoolSize = poolSize;
        }

        public async Task<T> Get()
        {
            await _semaphore.WaitAsync(_ct);

            T instance;
            _objects.TryTake(out instance);
            if (instance == null)
            {
                instance = await _objectGenerator();
                instance.InstanceId = Interlocked.Increment(ref _instanceCount);
            }
            return instance;
        }

        public async Task InitializePool(CancellationToken ct)
        {
            if (_objects.Count > 0)
                throw new Exception("ObjectPool already initialized.");

            await Parallel.ForEachAsync(Enumerable.Range(1, _maxPoolSize),
                new ParallelOptions { MaxDegreeOfParallelism = _maxPoolSize, CancellationToken = ct },
                async (i, ct) =>
                {
                    T instance = await _objectGenerator();
                    instance.InstanceId = Interlocked.Increment(ref _instanceCount);
                    _objects.Add(instance);
                });
        }

        public List<T> GetAll()
        {
            return _objects.ToList();
        }

        public void Return(T item)
        {
            _objects.Add(item);
            _semaphore.Release();
        }
    }
}