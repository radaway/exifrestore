using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ExifRestore
{
    public class ExifProcess : PoolableObject
    {
        readonly ILogger _logger;
        readonly Process _process;
        readonly CancellationToken _ct;

        private ExifProcess(Process process, ILogger logger, CancellationToken ct)
        {
            _process = process;
            _logger = logger;
            _ct = ct;
        }

        public static async Task<ExifProcess> Create(ILogger logger, CancellationToken ct)
        {
            var process = new Process();
            var exifProcess = new ExifProcess(process, logger, ct);
            process.StartInfo.FileName = Path.Combine(Environment.CurrentDirectory, "exiftool", "exiftool.exe");
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardInput = true;

            process.OutputDataReceived += (sender, data) =>
            {
                if (!string.IsNullOrWhiteSpace(data.Data))
                {
                    logger.LogInformation($"exiftool ({exifProcess.InstanceId}): {data.Data}");
                    if (data.Data.StartsWith("{ready"))
                    {
                        if (data.Data == "{ready}")
                        {
                            exifProcess.Initialized = true;
                            logger.LogInformation($"initialized exiftool ({exifProcess.InstanceId})");
                        }
                        exifProcess.IsReady = true;
                    }
                }
            };
            process.ErrorDataReceived += (sender, data) =>
            {
                if (!string.IsNullOrWhiteSpace(data.Data))
                {
                    logger.LogError($"exiftool ({exifProcess.InstanceId}) error: {data.Data}");
                }
            };
            process.StartInfo.Arguments = " -stay_open true -@ -";

            process.Start();
            ChildProcessTracker.AddProcess(process);
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.StandardInput.AutoFlush = true;
            await Task.Delay(1000); //not great not terrible
            await process.StandardInput.WriteLineAsync("-execute");
            //await exifProcess.WaitForExitAsync();//no cancellationtoken to let process end normally
            return exifProcess;
        }

        async Task WaitForReady()
        {
            while (!IsReady)
                await Task.Delay(10);
        }

        public async Task SendCommand(List<string> command, int counter)
        {
            if (command.Count < 1)
                return;
            await WaitForReady();
            IsReady = false;
            foreach (var line in command)
                await _process.StandardInput.WriteLineAsync(line);
            _logger.LogInformation($"#{counter}) exiftool ({InstanceId}) command: {string.Join(" ", command)}");
            await _process.StandardInput.WriteLineAsync($"-execute{counter}");
            await WaitForReady();
        }

        public void SendCtrlC()
        {
            _process.StandardInput.WriteLine((char)3); //ctrl-c
        }

        public async Task SendCtrlCAsync()
        {
            await _process.StandardInput.WriteLineAsync((char)3); //ctrl-c
        }
    }
}