﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System.Threading;
using System.Linq;
using ILogger = Microsoft.Extensions.Logging.ILogger;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace ExifRestore
{
    internal class Program
    {
        static Regex jsonRegex = new Regex(@"(\(\d{1}\))\.json");
        static ILogger _logger;
        static ObjectPool<ExifProcess> _pool;
        static ConcurrentDictionary<string, byte> _processedFiles = new ConcurrentDictionary<string, byte>();
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            _logger = serviceProvider.GetService<ILogger<Program>>();

            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var configuration = builder.Build();

            string dateTimeFormat = configuration["dateTimeFormat"];
            string startLocation = configuration["startLocation"];
            int maxDegreeOfParallelism = (int)await CSharpScript.EvaluateAsync(configuration["maxDegreeOfParallelism"]);
            int maxExifToolInstances = (int)await CSharpScript.EvaluateAsync(configuration["maxExifToolInstances"]);
            bool preInitExifInstances = bool.Parse(configuration["preInitExifInstances"]);

            using var cts = new CancellationTokenSource();
            System.AppDomain.CurrentDomain.ProcessExit += (s, e) =>
            {
                _logger.LogInformation("Process exiting");
            };

            Console.CancelKeyPress += (s, ev) =>
            {
                _logger.LogInformation("Ctrl+C pressed");
                cts.Cancel();
                foreach (var item in _pool.GetAll())
                {
                    item.SendCtrlCAsync();
                }
                ev.Cancel = true;
            };

            _logger.LogInformation($"Processor count: {Environment.ProcessorCount}");

            _pool = new ObjectPool<ExifProcess>(() => CreateExifToolInstance(cts.Token), maxExifToolInstances, cts.Token);
            await _pool.InitializePool(cts.Token);
            var dirStart = new DirectoryInfo(startLocation);
            int counter = 0;

            try
            {
                await Parallel.ForEachAsync(
                    dirStart.EnumerateFiles("*.json",
                        new EnumerationOptions() { RecurseSubdirectories = true, MaxRecursionDepth = Int32.MaxValue }).Where(x => !(x.Name.StartsWith("metadata") && x.Name.EndsWith(".json")) &&
                        !(x.Name.StartsWith("print-subscriptions") && x.Name.EndsWith(".json")) && !(x.Name.StartsWith("shared_album_comments") && x.Name.EndsWith(".json")) &&
                        !(x.Name.StartsWith("user-generated-memory-titles") && x.Name.EndsWith(".json"))),
                    new ParallelOptions { MaxDegreeOfParallelism = maxDegreeOfParallelism, CancellationToken = cts.Token },
                    async (fi, ct) =>
                    {
                        var jsonFileName = fi.Name;
                        var info = await ReadJsonFile(fi.FullName, dateTimeFormat);
                        var fileName = info.title;
                        var matchJsonFileName = jsonRegex.Match(jsonFileName);
                        var match = jsonRegex.Match(jsonFileName);
                        if (match.Success) {
                            var capture = match.Groups[1].Value;
                            var idx = fileName.LastIndexOf(".");
                            fileName = fileName.Insert(idx, capture);
                        }

                        var fullFileName = Path.Combine(fi.DirectoryName, fileName);
                        var exists = File.Exists(fullFileName);
                        var _counter = Interlocked.Increment(ref counter);
                        _logger.LogInformation($"#{_counter}) {fi.FullName} - {fileName} : {exists}");
                        if (!exists)
                        {
                            TryFixFilename(fi.DirectoryName, fi.Name, fileName);
                            exists = File.Exists(fullFileName);
                        }

                        if (exists)
                        {
                            try
                            {
                                var processing = _processedFiles.TryAdd(fullFileName, byte.MinValue);
                                if (processing)
                                {
                                    _logger.LogInformation($"#{_counter}) Setting exif for: {fullFileName}");
                                    await ProcessFile(fullFileName,
                                        info.creationTime.formatted.ToString("yyyy:M:d H:m:s"),
                                        info.photoTakenTime.formatted.ToString("yyyy:M:d H:m:s"),
                                        info.geoData.latitude.ToString(), info.geoData.longitude.ToString(), info.geoData.altitude.ToString(), ct, _counter);
                                } else {
                                    _logger.LogWarning($"#{_counter}) {fullFileName} already processed.");
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex, $"#{_counter}) Failed to set exif for: {fullFileName}");
                            }
                        }
                    });
            }
            catch (TaskCanceledException tce)
            {

            }
        }

        static void TryFixFilename(string directory, string jsonName, string fileName)
        {
            if (string.IsNullOrEmpty(fileName) || !fileName.Contains("."))
            {
                _logger.LogError($"Filename weirdness, look into it: {directory}\\{jsonName}");
                return;
            }
            var splitFileName = fileName.Split(".");
            jsonName = jsonName.Replace(".json", "");
            var idx = jsonName.LastIndexOf(".");
            jsonName = idx < 0 ? jsonName : jsonName.Substring(0, idx);

            var dir = new DirectoryInfo(directory);
            var files = dir.EnumerateFiles($"{jsonName}*.{splitFileName.Last()}").Where(x => !x.Name.Contains("-edited.") && !x.Name.Contains("-edited("));
            var fileCount = files.Count();
            if (fileCount == 1)
            {
                var file = files.First();
                _logger.LogInformation($"Renaming file {file.Name} to {fileName}");
                try
                {
                    file.MoveTo(Path.Combine(directory, fileName), false);
                }
                catch (FileNotFoundException fnfe)
                {
                    //sometimes another thread will process the rename based on another json and this file will already be renamed
                }
                catch (IOException ie)
                {
                    //same as above
                }
            }
            else if (fileCount > 1)
            {
                _logger.LogError($"Found multiple files when searching for {jsonName}*.{splitFileName.Last()} in {directory}");
            }
            else
            {
                _logger.LogError($"Found 0 files when searching for {jsonName}*.{splitFileName.Last()} in {directory}");
            }
        }

        static async Task<PhotoInfo> ReadJsonFile(string fileName, string dateTimeFormat)
        {
            var options = new JsonSerializerOptions
            {
                NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.AllowNamedFloatingPointLiterals
            };
            options.Converters.Add(new DateTimeConverterUsingDateTimeParse(dateTimeFormat));

            using var fileStream = File.OpenRead(fileName);
            return await JsonSerializer.DeserializeAsync<PhotoInfo>(fileStream, options);
        }

        static async Task<ExifProcess> CreateExifToolInstance(CancellationToken ct)
        {
            try
            {

                return await ExifProcess.Create(_logger, ct);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during CreateExifToolInstance");
                return null;
            }
        }


        static async Task ProcessFile(string fileName, string createDate, string takenDate, string latitude, string longitude, string altitude, CancellationToken ct, int counter)
        {
            var exifProcess = await _pool.Get();
            try
            {

                var command = new List<string>();
                var extension = Path.GetExtension(fileName);
                var altFileName = fileName.Replace(extension, "*-edited*" + extension);
                switch (extension.ToLowerInvariant())
                {
                    case ".mp4":
                    case ".avi":
                    case ".mov":
                        command.Add($" -DateTimeOriginal=\"{takenDate}\"");
                        command.Add($" -CreateDate=\"{takenDate}\"");
                        command.Add($" -XMP-exif:DateTimeOriginal=\"{takenDate}\"");
                        command.Add($" -QuickTime:CreateDate=\"{takenDate}\"");
                        command.Add($" -System:FileCreateDate=\"{takenDate}\"");

                        command.Add($" -FileModifyDate=\"{createDate}\"");
                        command.Add($" -System:FileModifyDate=\"{createDate}\"");
                        command.Add($" -QuickTime:ModifyDate=\"{createDate}\"");

                        command.Add($" -Keys:GPSCoordinates=\"{latitude}, {longitude}, {altitude}\"");
                        command.Add($" -Userdata:GPSCoordinates=\"{latitude}, {longitude}, {altitude}\"");
                        command.Add($" -Itemlist:GPSCoordinates=\"{latitude}, {longitude}, {altitude}\"");
                        command.Add($" -XMP-exif:GPSLatitude=\"{latitude}\"");
                        command.Add($" -XMP-exif:GPSLongitude=\"{longitude}\"");
                        command.Add($" -XMP-exif:GPSAltitude=\"{altitude}\"");
                        command.Add(" -progress");
                        command.Add(" -overwrite_original");
                        command.Add($" {fileName}");
                        break;
                    default:
                        command.Add($" -DateTimeOriginal=\"{takenDate}\"");
                        command.Add($" -CreateDate=\"{takenDate}\"");
                        command.Add($" -System:FileCreateDate=\"{takenDate}\"");

                        command.Add($" -FileModifyDate=\"{createDate}\"");
                        command.Add($" -System:FileModifyDate=\"{createDate}\"");

                        command.Add($" -GPSLatitude=\"{latitude}\"");
                        command.Add($" -GPSLongitude=\"{longitude}\"");
                        command.Add($" -GPSAltitude=\"{altitude}\"");
                        command.Add(" -progress");
                        command.Add(" -overwrite_original");
                        command.Add($" {fileName}");
                        command.Add($" {altFileName}");
                        break;
                }
                await exifProcess.SendCommand(command, counter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during ProcessFile");
            }
            finally
            {
                _pool.Return(exifProcess);
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            var serilogLogger = new LoggerConfiguration()
            .Enrich.WithThreadId()
            .Enrich.WithProcessId()
            .Enrich.WithProcessName()
            .WriteTo.File(@"logs\exifRestore.log", outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} ({ThreadId}) [{Level:u3}] {Message:lj}{NewLine}{Exception}")
            .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} ({ThreadId}) [{Level:u3}] {Message:lj}{NewLine}{Exception}")
            .CreateLogger();

            services.AddLogging(configure => configure.AddSerilog(logger: serilogLogger, dispose: true))
                        .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Information);

        }
    }
}