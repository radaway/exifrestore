using System;
using System.Diagnostics;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ExifRestore
{
    public class DateTimeConverterUsingDateTimeParse : JsonConverter<DateTime>
    {
        string _defaultFormat;

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            Debug.Assert(typeToConvert == typeof(DateTime));
            var res = reader.GetString() != null ? reader.GetString() : string.Empty;
            return DateTime.ParseExact(res, _defaultFormat, CultureInfo.CurrentCulture, DateTimeStyles.AssumeUniversal);
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }

        public DateTimeConverterUsingDateTimeParse(string defaultFormat)
        {
            _defaultFormat = defaultFormat;
        }
    }
}